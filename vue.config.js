module.exports = {
	lintOnSave: false,
	css: {
		loaderOptions: {
			sass: {
				prependData: `@import '~@/common/styles/variables';`
			}
		}
	},
	devServer: {
		host: '0.0.0.0',
		port: 8081
	},
	productionSourceMap: false
}
